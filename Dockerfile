FROM node:alpine
WORKDIR /opt/app
COPY . .
RUN npm install
CMD ["node", "app.js"]
